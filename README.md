Minimal reproduction of bug when doing a `FetchContent` or `ExternalProject_add` 
during `git rebase`.

## Steps to reproduce

```bash
git clone https://gitlab.kitware.com/Ram-Z/gitrebasebug
cd gitrebasebug
git rebase --root --exec="mkdir -p build && cd build && cmake .."
```

## Expected result

The same result as running `mkdir -p build && cd build && cmake` while not being 
in a `git rebase`, i.e. `build/_deps/cmake-src` directory should contain a clone 
of the repository described in `FetchContent`.

## Actual result

The exec part of `git rebase` fails with the following output:

```
[ 22%] Performing download step (git clone) for 'cmake-populate'
fatal: working tree '/home/sb/src/git-rebase-env' already exists.
fatal: working tree '/home/sb/src/git-rebase-env' already exists.
fatal: working tree '/home/sb/src/git-rebase-env' already exists.
-- Had to git clone more than once:
          3 times.
CMake Error at cmake-subbuild/cmake-populate-prefix/tmp/cmake-populate-gitclone.cmake:66 (message):
  Failed to clone repository:
  'https://gitlab.kitware.com/cmake/dashboard-scripts'


make[2]: *** [CMakeFiles/cmake-populate.dir/build.make:91: cmake-populate-prefix/src/cmake-populate-stamp/cmake-populate-download] Error 1
make[1]: *** [CMakeFiles/Makefile2:73: CMakeFiles/cmake-populate.dir/all] Error 2
make: *** [Makefile:84: all] Error 2

CMake Error at /usr/share/cmake-3.13/Modules/FetchContent.cmake:798 (message):
  Build step for cmake failed: 2
Call Stack (most recent call first):
  /usr/share/cmake-3.13/Modules/FetchContent.cmake:889 (__FetchContent_directPopulate)
  CMakeLists.txt:11 (FetchContent_Populate)


-- Configuring incomplete, errors occurred!
See also "/home/sb/src/git-rebase-env/build/CMakeFiles/CMakeOutput.log".
warning: execution failed: mkdir -p build && cd build && cmake .. && make
You can fix the problem, and then run

  git rebase --continue
```

It seems that during a `git rebase` the following environment variables are set:

```
GIT_WORK_TREE=/path/to/repo
GIT_DIR=/path/to/repo/.git
```

These are then inherited by the `git` process started for `FetchContent` causing 
confusion.

## Workaround

This can be avoided by unsetting these variables when running `cmake`.
```bash
git rebase --root --exec="mkdir -p build && cd build && env -u GIT_WORK_TREE -u GIT_DIR cmake .."
```
